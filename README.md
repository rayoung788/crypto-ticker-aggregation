# Cypto-Ticker-Aggregation

## Summary

The goal is to test the statement at the end of http://www.usv.com/blog/fat-protocols : 
> the market cap of the protocol always grows faster than the combined value of the applications built on top, since the success of the application layer drives further speculation at the protocol layer.

Demo here: http://23.24.168.98/crypto-ticker-aggregation/

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build
```

For detailed explanation on how things work, consult the [docs for vue-loader](http://vuejs.github.io/vue-loader).

## Historical View
Currently displays **market cap** over the **past seven days** using https://github.com/CoinCapDev/CoinCap.io.

 *The history view is currently hidden*.

## Current View
Currently displays the current **market cap**  using http://coincap.io/front.

## How to use
1. Open the built index.html in your browser
2. Clicking on elements in the legend temporarily removes them from the plot.
3. See frame.html for an example of how to inlcude this component in an iframe.

## Caveats
1. Not all of the tickers have data for the **past seven days**. If you notice that tickers are not being ploted, this is probably why. Double check that the API actually has data for the ticker you want to plot. The default set of tickers will work.
2. Times are not synchronized between tickers. They seem to be off by between a couple seconds to over a minute. Therefore, the times on the x-axis are general estimates.
3. Ticker colors are chosen randomly.
