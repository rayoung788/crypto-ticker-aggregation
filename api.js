const axios = require('axios')
const fs = require('fs')

const standalone = ['ETH']

const aggregate = [
  'OMG',
  'QTUM',
  'EOS',
  'MRK',
  'GNT',
  'REP',
  'TENX',
  'BAT',
  'KNC',
  'MTL',
  'ICN',
  'DGN',
  'CIV',
  'ZRX',
  'SNT',
  'SNGLS',
  'FUN',
  'BNT',
  'GNO',
  'MCO',
  'BQX',
  'ANT',
  'TAAS',
  'WINGS',
  'MLN',
  'DNT',
  'RLC',
  'EGD',
  'STORJ',
  'TRST',
  'CFI',
  'TKN',
  'MANA',
  'XAURUM',
  'ROL',
  'ST',
  'NMR',
  'ADT',
  'SAN',
  'TIME',
  'LUN',
  'HUM',
  'Guppy',
  'SWT',
  'BCAP',
  'vSlice',
  'NET',
  'PLU'
]

const coinValidation = data => {
  const fetchedCoins = data.map(coin => {
    return coin.short
  })
  const validStandalone = standalone.filter(coin => {
    return fetchedCoins.includes(coin)
  })
  const validAggregate = aggregate.filter(coin => {
    return fetchedCoins.includes(coin)
  })
  const invalidCoins = [...standalone, ...aggregate].filter(coin => {
    return !fetchedCoins.includes(coin)
  })
  return [validStandalone, validAggregate, invalidCoins]
}

const coincapFront = () => {
  return axios.get(`http://coincap.io/front`).then(response => {
    const timestamp = new Date().toString()
    const [validStandalone, validAggregate, invalidCoins] = coinValidation(
      response.data
    )
    fs.appendFile(
      './log.txt',
      `${timestamp} | Invalid Coins: ${JSON.stringify(invalidCoins)}\n`
    )
    fs.writeFile(
      './tickers.js',
      `window.tickers = {
    date: "${timestamp}",
    standalone: ${JSON.stringify(validStandalone)},
    aggregate: ${JSON.stringify(validAggregate)},
    cache: ${JSON.stringify(response.data)}
};
`,
      () => console.log('Coin Fetch Complete')
    )
  })
}

coincapFront()
