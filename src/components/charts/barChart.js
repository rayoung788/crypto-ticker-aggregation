import { Bar, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default Bar.extend({
  mixins: [reactiveProp],
  props: ['options'],
  data() {
    return {
      extras: {
        scales: {
          yAxes: [
            {
              scaleLabel: {
                display: true,
                labelString: 'Market Cap'
              },
              ticks: {
                callback: function(value, index, values) {
                  return '$' + value.toLocaleString('en-US', { maximumFractionDigits: 2 })
                }
              }
            }
          ],
          xAxes: [
            {
              barPercentage: 1.0
            }
          ]
        },
        legend: {
          display: false
        },
        tooltips: {
          callbacks: {
            label: this.tooltipLabels,
            title: function(tooltipItems, data) {
              return ''
            }
          }
        }
      }
    }
  },
  mounted () {
    this.renderChart(this.chartData, {...this.options, ...this.extras})
  },
  methods: {
    tooltipLabels(tooltipItems, data) {
      let label = data.labels[tooltipItems.index]
      let value = data.datasets[0].data[tooltipItems.index]
      let total = data.datasets[0].total
      return `${label}: $${value.toLocaleString('en-US', {
        maximumFractionDigits: 2
      })} (${(value / total * 100).toFixed(2)}%)`
    }
  }
})
