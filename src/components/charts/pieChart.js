import { Pie, mixins } from 'vue-chartjs'
const { reactiveProp } = mixins

export default Pie.extend({
  mixins: [reactiveProp],
  props: ['options'],
  data() {
    return {
      tooltips: {
        tooltips: {
          callbacks: {
            label: this.tooltipLabels
          }
        }
        // events: false,
        // animation: {
        //   duration: 500,
        //   easing: 'easeOutQuart',
        //   onComplete: function(context) {
        //     let ctx = context.chart.ctx
        //     ctx.textAlign = 'center'
        //     ctx.textBaseline = 'bottom'

        //     this.data.datasets.forEach(function(dataset) {
        //       for (var i = 0; i < dataset.data.length; i++) {
        //         var model =
        //             dataset._meta[Object.keys(dataset._meta)[1]].data[i]._model,
        //           total = dataset._meta[Object.keys(dataset._meta)[1]].total,
        //           mid_radius =
        //             model.innerRadius +
        //             (model.outerRadius - model.innerRadius) / 2,
        //           start_angle = model.startAngle,
        //           end_angle = model.endAngle,
        //           mid_angle = start_angle + (end_angle - start_angle) / 2

        //         var x = mid_radius * Math.cos(mid_angle)
        //         var y = mid_radius * Math.sin(mid_angle)

        //         ctx.fillStyle = '#fff'
        //         if (i == 3) {
        //           // Darker text color for lighter background
        //           ctx.fillStyle = '#444'
        //         }
        //         var percent = `${(dataset.data[i] / total * 100).toFixed(2)}%`
        //         ctx.fillText(percent, model.x + x, model.y + y + 15)
        //       }
        //     })
        //   }
        // }
      }
    }
  },
  mounted() {
    this.renderChart(this.chartData, { ...this.options, ...this.tooltips })
  },
  methods: {
    tooltipLabels(tooltipItems, data) {
      let label = data.labels[tooltipItems.index]
      let value = data.datasets[0].data[tooltipItems.index]
      let total = data.datasets[0].total
      return `${label}: $${value.toLocaleString('en-US', {
        maximumFractionDigits: 2
      })} (${(value / total * 100).toFixed(2)}%)`
    }
  }
})
